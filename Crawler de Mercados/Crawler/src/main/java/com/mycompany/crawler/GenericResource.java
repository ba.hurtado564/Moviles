/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.crawler;

import java.io.IOException;
import java.util.HashSet;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * REST Web Service
 *
 * @author Javier
 */
@Path("crawler")
public class GenericResource {
    
    String urlFree = "https://play.google.com/store/apps/collection/topselling_free";
    String urlPaid = "https://play.google.com/store/apps/collection/topselling_paid";
    String urlEditor  = "https://play.google.com/store/apps/topic?id=editors_choice";
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of GenericResource
     */
    public GenericResource() {
    }

    /**
     * Retrieves representation of an instance of com.mycompany.crawler.GenericResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public String getXml() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

        @GET
        @Path("tops")
        @Produces(MediaType.APPLICATION_JSON)    
        public Response getTops() {
        //TODO return proper representation object
  return Response.status(201).entity(loadTops()).build();
    }
           @GET
        @Path("top10reviewsnumber")
        @Produces(MediaType.APPLICATION_JSON)    
        public Response getTop10reviewsnumber() {
        //TODO return proper representation object
  return Response.status(201).entity(getTop10RaitingsNumber(urlFree)).build();
    }
        
        
                   @GET
        @Path("top10scoresnumber")
        @Produces(MediaType.APPLICATION_JSON)    
        public Response getTop10scorenumber() {
        //TODO return proper representation object
  return Response.status(201).entity(   getTop10reviews(urlFree) ).build();
    }
        
        
                  @GET
        @Path("top10reviewsnumberpaid")
        @Produces(MediaType.APPLICATION_JSON)    
        public Response getTop10reviewsnumberPaid() {
        //TODO return proper representation object
  return Response.status(201).entity(getTop10RaitingsNumber(urlPaid)).build();
    }
        
        
        @GET
        @Path("top10scoresnumberpaid")
        @Produces(MediaType.APPLICATION_JSON)    
        public Response getTop10scorenumberPaid() {
        //TODO return proper representation object
        String algo  =getTop10reviews(urlPaid);
       System.out.println(algo);
  return Response.status(201).entity(   algo ).build();
    }
        
     
    /**
     * PUT method for updating or creating an instance of GenericResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public void putXml(String content) {
    }
    
    
    public String loadTops() {
        try {

        String urls[] = new String[3];
        urls[0]=urlFree;
        urls[1]=urlPaid;
        urls[2]=urlEditor;
        JSONObject papitas = new JSONObject();
                   for (int i = 0; i < urls.length; i++) {
                   String initialURL = urls[i];
                   	Document doc = Jsoup.connect(initialURL).timeout(0).get();
        Document detail = null;
                        HashSet<String> refs = new HashSet<String>();
                        Elements anchords = doc.getElementsByClass("card-click-target");
			String url =  "https://play.google.com/"+ anchords.get(0).attr("href");
                        detail  = Jsoup.connect(url).timeout(0).get();
                        String name = detail.select("[class='id-app-title']").text();
                        
                        if(i==0)
                        {
                                papitas.put("free",name);

                        }
                        else if( i ==1)
                        {
                                papitas.put("paid",name);

                        }
                        else
                        {
                               papitas.put("editor",name);

                        }
               }
               return papitas.toString();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
                       return "ERROR";

	}
    
    
    
        public String getTop10RaitingsNumber(String ur) {
            try{

        JSONArray   arr = new JSONArray();
        Document doc = Jsoup.connect(ur).timeout(0).get();
        Document detail = null;
                        HashSet<String> refs = new HashSet<String>();
                        Elements anchords = doc.getElementsByClass("card-click-target");
			
                        for (int i = 0; i < 40; i+=4) {
                                    JSONObject papitas = new JSONObject();

                      String url =  "https://play.google.com/"+ anchords.get(i).attr("href");
                        detail  = Jsoup.connect(url).timeout(0).get();
                        String name = detail.select("[class='id-app-title']").text();
                        String reviews = detail.select("[class='rating-count']").text();
                        String score = detail.select("[class='score']").text();
                        papitas.put("name",name);
                        papitas.put("reviews",reviews.replaceAll(",", ""));
                        arr.put(papitas);
                }
                        return arr.toString();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
                       return "ERROR";
        
        }
         public String getTop10reviews(String ur) {
            try{

        JSONArray   arr = new JSONArray();
        Document doc = Jsoup.connect(ur).timeout(0).get();
        Document detail = null;
                        HashSet<String> refs = new HashSet<String>();
                        Elements anchords = doc.getElementsByClass("card-click-target");
			
                        for (int i = 0; i < 40; i+=4) {
                                    JSONObject papitas = new JSONObject();

                      String url =  "https://play.google.com/"+ anchords.get(i).attr("href");
                        detail  = Jsoup.connect(url).timeout(0).get();
                        String name = detail.select("[class='id-app-title']").text();
                        String score = detail.select("[class='score']").text();

                        papitas.put("name",name);
                                                papitas.put("score",score.replaceAll(",", "."));

                                               arr.put(papitas);

                }
                      

                 
               
               return arr.toString();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
                       return "ERROR";
        
        }
         
         
    
    
}
