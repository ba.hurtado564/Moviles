//AJAX METHOD
self.ajax = function(uri, method, data) {
    var request = {
        url: uri,
        type: method,
        contentType: "application/json",
        accepts: "application/json",
        cache: false,
        dataType: 'json',
        data: JSON.stringify(data),
        beforeSend: function (xhr) {
//                    xhr.setRequestHeader("Authorization",
//                        "Basic " + btoa(self.username + ":" + self.password));
        },
        error: function(jqXHR) {
            console.log("ajax error " + jqXHR.status +" and "+ jqXHR);
        }
    };
    return $.ajax(request);

};
$(document).ready(function () {
var stringGET = "/Crawler/webresources/crawler/tops";
self.ajax(stringGET, 'GET').done(function(data) {
console.log(data)
$('#googleEditor').html(data.editor);
$('#topPaid').html(data.paid);
$('#topFree').html(data.free);

});


var stringGET = "/Crawler/webresources/crawler/top10reviewsnumber";
self.ajax(stringGET, 'GET').done(function(data) {
console.log(data)

var chart = AmCharts.makeChart("chartTop10FreeApps", {
  "type": "serial",
  "theme": "none",
  "marginRight": 70,
  "dataProvider": data,
  "valueAxes": [{
    "axisAlpha": 0,
    "position": "left",
  }],
  "startDuration": 1,
  "graphs": [{
    "balloonText": "<b>[[category]]: [[value]]</b>",
    "fillColorsField": "color",
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "reviews"
  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "name",
  "categoryAxis": {
    "gridPosition": "start",
    "labelRotation": 45
  },
  "export": {
    "enabled": true
  }
});
});

var stringGET = "/Crawler/webresources/crawler/top10scoresnumber";
self.ajax(stringGET, 'GET').done(function(data) {
console.log(data)

var chart = AmCharts.makeChart("chartTopscore10FreeApps", {
  "type": "serial",
  "theme": "light",
  "marginRight": 70,
  "dataProvider": data,
  "valueAxes": [{
    "axisAlpha": 0,
    "position": "left",
  }],
  "startDuration": 1,
  "graphs": [{
    "balloonText": "<b>[[category]]: [[value]]</b>",
    "fillColorsField": "color",
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "score"
  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "name",
  "categoryAxis": {
    "gridPosition": "start",
    "labelRotation": 45
  },
  "export": {
    "enabled": true
  }

});

});



var stringGET = "/Crawler/webresources/crawler/top10reviewsnumberpaid";
self.ajax(stringGET, 'GET').done(function(data) {
console.log(data)

var chart = AmCharts.makeChart("chartTop10PaidApps", {
  "type": "serial",
  "theme": "none",
  "marginRight": 70,
  "dataProvider": data,
  "valueAxes": [{
    "axisAlpha": 0,
    "position": "left",
  }],
  "startDuration": 1,
  "graphs": [{
    "balloonText": "<b>[[category]]: [[value]]</b>",
    "fillColorsField": "color",
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "reviews"
  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "name",
  "categoryAxis": {
    "gridPosition": "start",
    "labelRotation": 45
  },
  "export": {
    "enabled": true
  }
});

});

var stringGET = "/Crawler/webresources/crawler/top10scoresnumberpaid";
self.ajax(stringGET, 'GET').done(function(data) {
console.log(data)

var chart = AmCharts.makeChart("chartTopscore10PaidApps", {
  "type": "serial",
  "theme": "light",
  "marginRight": 70,
  "dataProvider": data,
  "valueAxes": [{
    "axisAlpha": 0,
    "position": "left",
  }],
  "startDuration": 1,
  "graphs": [{
    "balloonText": "<b>[[category]]: [[value]]</b>",
    "fillColorsField": "color",
    "fillAlphas": 0.9,
    "lineAlpha": 0.2,
    "type": "column",
    "valueField": "score"
  }],
  "chartCursor": {
    "categoryBalloonEnabled": false,
    "cursorAlpha": 0,
    "zoomable": false
  },
  "categoryField": "name",
  "categoryAxis": {
    "gridPosition": "start",
    "labelRotation": 45
  },
  "export": {
    "enabled": true
  }

});

});


});
