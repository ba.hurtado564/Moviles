//
//  Child_MonitorTests.swift
//  Child MonitorTests
//
//  Created by Javier Camargo Urrego on 10/17/17.
//  Copyright © 2017 Javier Esteban Camargo Urrego. All rights reserved.
//

import XCTest

class Child_MonitorTests: XCTestCase {
    
    var usuariosTest:[Usuario] = [Usuario]()
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let usuario1 = Usuario ([
            "id": "1" as AnyObject,
            "login": "prueba" as AnyObject,
            "nombre": "Manolo" as AnyObject,
            "apellido": "Doe" as AnyObject,
            "telefono": "6666666666" as AnyObject,
            "passwd": "asdasd1" as AnyObject

            ])

        
        let usuario2 = Usuario ([
            "id": "2" as AnyObject,
            "login": "prueba2" as AnyObject,
            "nombre": "Jack" as AnyObject,
            "apellido": "Daniels" as AnyObject,
            "telefono": "12341234" as AnyObject,
            "passwd": "123456" as AnyObject
            ])
        usuariosTest.append(usuario1)
        print(usuario1.id!)

        usuariosTest.append(usuario2)

    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssert(true)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            var procesados = 0
            for usuarioTemp in self.usuariosTest {
                print(usuarioTemp.nombre!)
                procesados = procesados + 1
            }
            //XCTAssert(procesados == 3, "Deberia fallar")
            XCTAssert(procesados == 2, "Sucess")

            // Put the code you want to measure the time of here.
        }
    }
    
}
