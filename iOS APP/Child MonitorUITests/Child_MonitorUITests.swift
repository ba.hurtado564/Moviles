//
//  Child_MonitorUITests.swift
//  Child MonitorUITests
//
//  Created by Javier Camargo Urrego on 10/17/17.
//  Copyright © 2017 Javier Esteban Camargo Urrego. All rights reserved.
//

import XCTest

class Child_MonitorUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        
        
        let app = XCUIApplication()
        app.buttons["Create Account"].tap()
        
        let element = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element
        let textField = element.children(matching: .textField).element(boundBy: 0)
        textField.tap()
        textField.typeText("jack")
        
        let textField2 = element.children(matching: .textField).element(boundBy: 1)
        textField2.tap()
        textField2.tap()
        textField2.typeText("daniels")
        
        let textField3 = element.children(matching: .textField).element(boundBy: 2)
        textField3.tap()
        textField3.tap()
        textField3.typeText("asd@gmail.com")
        
        let textField4 = element.children(matching: .textField).element(boundBy: 3)
        textField4.tap()
        textField4.tap()
        textField4.typeText("123456789")
        
        let secureTextField = element.children(matching: .secureTextField).element(boundBy: 0)
        secureTextField.tap()
        secureTextField.tap()
        secureTextField.typeText("asdfgh1")
        
        let secureTextField2 = element.children(matching: .secureTextField).element(boundBy: 1)
        secureTextField2.tap()
        secureTextField2.tap()
        secureTextField2.typeText("asdfg1")
        
        let registerButton = app.buttons["Register"]
        registerButton.tap()
        app.alerts["contraseñas no coinciden"].buttons["cerrar"].tap()
        secureTextField.tap()
        secureTextField.tap()
        secureTextField.typeText("asd123!")
        secureTextField2.tap()
        secureTextField2.tap()
        secureTextField2.tap()
        secureTextField2.typeText("asd123!")
        registerButton.tap()
        
        let cerrarButton = app.alerts["ÉXITO"].buttons["cerrar"]
        cerrarButton.tap()
        registerButton.tap()
        cerrarButton.tap()
        app.navigationBars["Register"].buttons["Stop"].tap()


        //Adding user, it goes to firebase and create it one, checking previues errors
    }
    
}
