//
//  MenuControllerFather.swift
//  Child Monitor
//
//  Created by Javier Camargo Urrego on 9/28/17.
//  Copyright © 2017 Javier Esteban Camargo Urrego. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import AFNetworking

var nombre: String = ""


class MenuControllerFather: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var ref: DatabaseReference!
    
    var son: [NSManagedObject] = []
    
    var usuario: Usuario!
    
    var hijos : [Hijo] = [Hijo] ()
    
    @IBAction func cambioMetodoLogin(_ sender: Any) {
    }
    @IBOutlet weak var listCell: UITableView!
    
    @IBAction func addChild(_ sender: Any) {
        
        self.performSegue(withIdentifier: "GoToAddChild", sender: usuario)
        
    }

    @IBAction func openActionSheet(_ sender: Any) {
        let alertActionSheet = UIAlertController (title: "options", message: "Child Monitor", preferredStyle: .actionSheet)
        let logOutAction = UIAlertAction(title: "Logout", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            defaults.set("no", forKey: "SeHanLogueado")
            self.dismiss(animated: true, completion: nil)
        })
        let refreshAction = UIAlertAction(title: "Refresh", style: .default, handler:
            {
                (alert: UIAlertAction!) -> Void in
                self.cargarHijos()
        })
        
        alertActionSheet.addAction(logOutAction)
        alertActionSheet.addAction(refreshAction)
        self.present(alertActionSheet, animated: true, completion: nil)
    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        
        listCell.delegate = self
        listCell.dataSource = self
        
        cargarHijos()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.cargarHijos()
        
    }
    
    func cargarHijos ( ) {
        
        self.hijos = self.usuario.hijos
        self.listCell.reloadData()
        
    }
    
    func downloadImage (url: URL, imageView: UIImageView){
        print ("Download started")
        
        DispatchQueue.global(qos: .userInteractive).async {
            
            self.getDataFromUrl(url: url){ (data, response, error) in
                guard let data = data, error == nil else {return}
                print(response?.suggestedFilename ?? url.lastPathComponent)
                print("Download Finished")
                DispatchQueue.main.async() { () -> Void in
                    imageView.image = UIImage(data:data)
                    
                }
                
            }
        }
        
        
    }
    
    func getDataFromUrl(url : URL, completion: @escaping (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if !usuario.tieneHijos(){
            self.performSegue(withIdentifier: "GoToAddChild", sender: usuario)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToAddChild"{
            let viewController: AddSonController = segue.destination as! AddSonController
            viewController.usuario = sender as! Usuario
            
        }
        if segue.identifier == "GoToChildDetails" {
            let viewController: SonDetailsController = segue.destination as!
            SonDetailsController
            viewController.son = sender as! Hijo
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return hijos.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "SonCell", for: indexPath)
        
        let currentHijo: Hijo = hijos[indexPath.row]
        
//        cell.textLabel!.text = hijos[indexPath.row].nombre
        
        let imageView: UIImageView = cell.viewWithTag(2) as! UIImageView
//        downloadImage(url: URL(string: currentHijo.imagen!)!, imageView: imageView)
//        imageView.setImageWith(URL(string: currentHijo.imagen!)!)
//
        let labelNombre: UILabel = cell.viewWithTag(1) as! UILabel
        labelNombre.text = currentHijo.nombre!
        
//        let labelMetodo: UILabel = cell.viewWithTag(3) as! UILabel
//        labelMetodo.text = String(currentHijo.TipoMonitoreo!)
        
        
        return cell
    }
    
//    func downloadImage (url: URL, imageView: UIImageView){
//        print ("Download started")
//        getDataFromUrl(url:url){ (data, response, error) in
//            guard let data = data, error == nil else {return}
//            print(response?.suggestesFilename ?? url.lastPathComponent)
//            print("Download Finished")
//            DispatchQueue.main.async() { () -> Void in
//                imageView.image = UIImage(data:data)
//                
//            }
//            
//        }
//    }
//    
//    func getDataFromUrl(url : URL, completion @escaping (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void) {
//        URLSession.shared.dataTask(with: url) {
//            (data, response, error) in
//            completion(data, response, error)
//        }.resume()
//    }
    
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let son = hijos[indexPath.row]
        let name = son.nombre!
        self.performSegue(withIdentifier: "GoToChildDetails", sender: son)
    }
    

    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
