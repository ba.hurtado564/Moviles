//
//  SonDetailsController.swift
//  Child Monitor
//
//  Created by Javier Camargo Urrego on 10/8/17.
//  Copyright © 2017 Javier Esteban Camargo Urrego. All rights reserved.
//

import UIKit
import CoreData
import Firebase

class SonDetailsController: UIViewController {
    
    
   @IBOutlet weak var NavBar: UINavigationBar!

    var son: Hijo!
    var name: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ENTRO")
        name = son.nombre!
        NavBar.topItem?.title = name
        
        
        // Do any additional setup after loading the view.
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func deleteChild(_ sender: Any)
    {
    /**
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let moc = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Son")
        
        let result = try? moc.fetch(fetchRequest)
        let resultData = result as! [Son]
        
        for object in resultData {
            if  object.name! == name
            {
                moc.delete(object)
                
            }
        }
        do {
            try moc.save()
            print("saved!")
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        } catch {
            
        }
        */
            
            
        self.dismiss(animated: true, completion: nil)
        
        
        
    }
    
    @IBAction func dismiss(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
