//
//  ViewController.swift
//  Child Monitor
//
//  Created by Javier Esteban Camargo Urrego on 25/09/17.
//  Copyright © 2017 Javier Esteban Camargo Urrego. All rights reserved.
//

import UIKit
import Firebase

extension UIViewController {
    
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

class ViewController: UIViewController,UITextFieldDelegate {
    
    var metodoSeleccionado: String = "Padre"
    
    typealias CompletionHandler = (_ success:Bool) -> Void

    struct ConstantsSegmented {
        let metodoLogin = 0
        let HijoLog = 1
    }
    
    var ref: DatabaseReference!
    
    @IBOutlet weak var emailTxt: UITextField!
    
    @IBOutlet weak var passwordTxt: UITextField!
    
    @IBOutlet weak var metodoLogin: UISegmentedControl!
    
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var createAccountBtn: UIButton!
    
    @IBAction func metodoLogin(_ sender: Any) {
        if metodoLogin.selectedSegmentIndex == ConstantsSegmented().metodoLogin {
                        metodoSeleccionado = "Padre"
                    } else if metodoLogin.selectedSegmentIndex ==
                        ConstantsSegmented().HijoLog {
                        metodoSeleccionado = "Hijo"
                    }
    }
    
    @IBAction func loginAction(_ sender: Any) {
        if emailTxt.text == nil || emailTxt.text == ""
        {
            showAlert(title: "Login vacío", message: "Ingresar login", closeButtonTitle: "cerrar")
            return
        }
        if passwordTxt.text == nil || passwordTxt.text == ""
        {
            showAlert(title: "Password vacío", message: "Ingresar password", closeButtonTitle: "cerrar")
            return
        }
        Auth.auth().signIn(withEmail: emailTxt.text!, password: passwordTxt.text!) { (user, error) in
            if let error = error {
                self.showAlert(title: "Error", message: error.localizedDescription, closeButtonTitle: "cerrar")
                return
            }
            
            if self.metodoSeleccionado == "Padre"{
                
                let usuarioActual: Usuario = Usuario( id: user!.uid, login: self.emailTxt.text!, passwd: self.passwordTxt.text!)
                self.cargarHijos(user: usuarioActual,completionHandler: { (success) -> Void in
                    
                    // When download completes,control flow goes here.
                    if success {
                        defaults.set("si", forKey: "SeHanLogueado")
                        defaults.set("padre", forKey: "Rol")
                        defaults.set(self.emailTxt.text, forKey: "User")
                        defaults.set(self.passwordTxt.text, forKey: "Passwd")
                        defaults.set(user!.uid, forKey: "UserId")
                        
                        self.performSegue(withIdentifier: "GoToMain", sender: usuarioActual)
                        
                    } else {
                        // download fail
                    }
                })
                
            }
            else {
                let usuarioActual: Usuario = Usuario( id: user!.uid, login: self.emailTxt.text!, passwd: self.passwordTxt.text!)
                self.cargarHijos(user: usuarioActual,completionHandler: { (success) -> Void in
                    
                    // When download completes,control flow goes here.
                    if success {
                        
                        if usuarioActual.hijos.count > 0{
                            defaults.set("si", forKey: "SeHanLogueado")
                            defaults.set("hijo", forKey: "Rol")
                            defaults.set(self.emailTxt.text, forKey: "User")
                            defaults.set(self.passwordTxt.text, forKey: "Passwd")
                            defaults.set(user!.uid, forKey: "UserId")
                            
                            self.performSegue(withIdentifier: "GoToConfigureSon", sender: usuarioActual)
                        }
                        else{
                            self.showAlert(title:"Error",message:"You have no registered children",closeButtonTitle: "close")
                        }
                        
                        
                    } else {
                        // download fail
                    }
                })
                
            }
            
            
        }
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    @IBAction func createAccount(_ sender: Any) {
        self.performSegue(withIdentifier: "GoToRegister", sender: nil)
        
    }
    
    func cargarHijos (user: Usuario, completionHandler: @escaping CompletionHandler) {
        let userID = Auth.auth().currentUser?.uid
        ref.child("Hijos").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if snapshot.exists(){
                let response =  snapshot.value as! [String: Any]
                for item in response{
                    let valores = item.value as! Dictionary<String,AnyObject>
                    let hijoActual: Hijo = Hijo (valores)
                    user.hijos.append(hijoActual)
                }
            }
            let flag = true // true if download succeed,false otherwise
            
            completionHandler(flag)
            
        }) { (error) in
            print(error.localizedDescription)
            let flag = false // true if download succeed,false otherwise
            
            completionHandler(flag)
        }
        
    }
    
    @objc func loginPadre() {
        
        
        let usuarioActual: Usuario = Usuario( id: defaults.string(forKey: "UserId")!, login: defaults.string(forKey: "User")!, passwd: defaults.string(forKey: "Passwd")!)
        cargarHijos(user: usuarioActual,completionHandler: { (success) -> Void in
            
            // When download completes,control flow goes here.
            if success {
                
                self.performSegue(withIdentifier: "GoToMain", sender: usuarioActual)
            }
            
            else {
                // download fail
            }
        })
    }
    
    @objc func loginHijo() {
        
        
        let usuarioActual: Usuario = Usuario( id: defaults.string(forKey: "UserId")!, login: defaults.string(forKey: "User")!, passwd: defaults.string(forKey: "Passwd")!)
        cargarHijos(user: usuarioActual,completionHandler: { (success) -> Void in
            
            // When download completes,control flow goes here.
            if success {
                
                var hijo = Hijo ()
                for item in usuarioActual.hijos {
                    if item.nombre == defaults.string(forKey: "NombreHijo"){
                        hijo = item
                    }
                }
                
                self.performSegue(withIdentifier: "GoToAlreadyConfiguredSon", sender: hijo)
            }
                
            else {
                // download fail
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToMain"{
            let viewController: MenuControllerFather = segue.destination as! MenuControllerFather
            viewController.usuario = sender as! Usuario
            
        }
        if segue.identifier == "GoToConfigureSon"{
            let viewController: ConfigureSonViewController = segue.destination as! ConfigureSonViewController
            viewController.usuario = sender as! Usuario
            
        }
        if segue.identifier == "GoToAlreadyConfiguredSon"{
            let viewController: MainSonViewController = segue.destination as! MainSonViewController
            viewController.hijo = sender as! Hijo
            
        }
    }
    
    
    override func viewDidLoad() {
        ref = Database.database().reference()
        super.viewDidLoad()
        
        //COMENTAR PARA HACER QUE PERSISTA
        defaults.set("no", forKey: "SeHanLogueado")
        
        loginBtn.layer.borderColor = UIColor.gray.cgColor
        loginBtn.layer.borderWidth = 1
        loginBtn.layer.cornerRadius = CGFloat(Float(20.0))
        loginBtn.layer.shadowRadius = CGFloat(Float(20.0))
        createAccountBtn.layer.borderColor = UIColor.gray.cgColor
        createAccountBtn.layer.borderWidth = 1
        createAccountBtn.layer.cornerRadius = CGFloat(Float(20.0))
        createAccountBtn.layer.shadowRadius = CGFloat(Float(20.0))

        
       // // Do any additional setup after loading the view, typically from a nib.
        self.emailTxt.delegate = self;
        self.passwordTxt.delegate = self;
        self.hideKeyboardWhenTappedAround()
        
        //Prueba Stress
        /**
        for index in 1...100000
        {
            DispactchQueue.global(gos: .userIntereactive).async {
            for index2 in 1...100000
            {
                print("Prueba" + index + " " + index2)
            }
            }
            
        }
 */

    }
    
    override func viewDidAppear(_ animated: Bool) {

        if defaults.string(forKey: "SeHanLogueado" ) == "si"{
            
            if defaults.string(forKey: "Rol" ) == "padre"{
                loginPadre()
            }
            else {
                loginHijo()
            }
        }
    }
    
    override func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func showAlert(title:String,message:String,closeButtonTitle:String)
    {
        let alertController = UIAlertController(title:title,message:message,preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: closeButtonTitle, style: .default)
        {
            (action:UIAlertAction) in
            //ALGO
            
        }
        alertController.addAction(cancelAction)
        self.present(alertController,animated: true)
        {
        }
        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Somthing for obtainin the type of role that the user have
    
    //Then perfom the segue for one story
    


}

