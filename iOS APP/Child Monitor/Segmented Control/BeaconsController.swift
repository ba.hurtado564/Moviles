//
//  BeaconsController.swift
//  Child Monitor
//
//  Created by Javier Esteban Camargo Urrego on 9/10/17.
//  Copyright © 2017 Javier Esteban Camargo Urrego. All rights reserved.
//

import UIKit
import CoreLocation

class BeaconsController: UIViewController, CLLocationManagerDelegate, UIPickerViewDataSource, UIPickerViewDelegate {

    
    var locationManager: CLLocationManager!
    let defaults = UserDefaults.standard

    
    @IBOutlet weak var txtRefreshRate: UITextField!
    @IBOutlet weak var txtRange: UITextField!
    @IBOutlet weak var beaconID: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager = CLLocationManager.init()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        startScanningForBeaconRegion(beaconRegion: getBeaconRegion())
        if  defaults.bool(forKey: "ULTIMO_ESTADO")
        {
         beaconID.text = "escaneando"
        }
    }
    
    
    @IBOutlet weak var asd: UILabel!
    @IBOutlet weak var lblacuracity: UILabel!
    @IBOutlet weak var lblProximity: UILabel!
    
    @IBOutlet weak var spinner: UIPickerView!
    let options = ["Immediate Proximity","Near Proximity","Far Proximity","Unknown Proximity"]
    var monitoreo: String!
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return options[row]
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return options.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        monitoreo = options[row]
    }
    
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        let beacon = beacons.last
        
        if beacons.count > 0 {
            beaconID.text = "Beacon Id: " + (beacon?.proximityUUID.uuidString)!
            lblacuracity.text = "Accuracity " + String(describing: beacon?.accuracy)
            
            
            if beacon?.proximity == CLProximity.unknown {
                lblProximity.text = "Unknown Proximity"
            } else if beacon?.proximity == CLProximity.immediate {
                lblProximity.text = "Immediate Proximity"
            } else if beacon?.proximity == CLProximity.near {
                lblProximity.text = "Near Proximity"
            } else if beacon?.proximity == CLProximity.far {
                lblProximity.text = "Far Proximity"
            }
            
            
            if (monitoreo != nil)
            {
// Terminal logica
                
            }

            
            

        } else {
            print("Nope")

        }
        
        print("Ranging")
    }
    
    func getBeaconRegion() -> CLBeaconRegion {
        let beaconRegion = CLBeaconRegion.init(proximityUUID: UUID.init(uuidString: "f89bb3fb-ce7a-4d8f-9b3e-127cc494b300")!,
                                               identifier: "MyRegion")
        return beaconRegion
    }
    
    func startScanningForBeaconRegion(beaconRegion: CLBeaconRegion) {
        print(beaconRegion)
        locationManager.startMonitoring(for: beaconRegion)
        locationManager.startRangingBeacons(in: beaconRegion)
        defaults.set(true, forKey: "ULTIMO_ESTADO")
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func showAlert(title:String,message:String,closeButtonTitle:String)
    {
        let alertController = UIAlertController(title:title,message:message,preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: closeButtonTitle, style: .default)
        {
            (action:UIAlertAction) in
        }
        alertController.addAction(cancelAction)
        self.present(alertController,animated: true)
        {
        }
    }
    @IBAction func dispose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
