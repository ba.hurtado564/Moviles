//
//  MenuController.swift
//  Child Monitor
//
//  Created by Javier Camargo Urrego on 9/27/17.
//  Copyright © 2017 Javier Esteban Camargo Urrego. All rights reserved.
//

import UIKit
import CoreLocation

class MenuControllerSon: UIViewController, CLLocationManagerDelegate {

    var locationManager: CLLocationManager = CLLocationManager ()
    var userLocation: CLLocation!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        userLocation=nil
        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var Localizacion: UILabel!

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let latestLocation: CLLocation = locations[locations.count - 1]
        
        if userLocation == nil{
            userLocation = latestLocation
            
        }
        Localizacion.text = "Ubicacion: (" +
        String(userLocation.coordinate.latitude) + "," +
        String(userLocation.coordinate.longitude) + ")"
        
     //   print("(" + String(userLocation.coordinate.latitude) + "," +
       //  String(userLocation.coordinate.longitude) + ")")
        
        
        
        func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
        {
            print("Location Manager Error: " + error.localizedDescription )
        }
        
    }
    
    
    
    @IBAction func sharus(_ sender: Any) {
        
        let msg = "Mensaje de Prueba"
        let urlWhats = "whatsapp://send?text=\(msg)"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        {
            if let whatsappURL = URL(string:urlString){
                if(UIApplication.shared.canOpenURL(whatsappURL))
                {
                    UIApplication.shared.open(whatsappURL, options: [:], completionHandler: {
                        (completed: Bool) in
                    })
                }else
                {
                    print("No se puede abrir WA")
                }
            }
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
