//
//  Usuario.swift
//  Child Monitor
//
//  Created by Nicolas Ramirez on 7/10/17.
//  Copyright © 2017 Javier Esteban Camargo Urrego. All rights reserved.
//

import UIKit

class Usuario: NSObject {
    var id: String!
    var login: String!
    var nombre: String!
    var apellido: String!
    var telefono: String!
    var passwd: String!
    var hijos = [Hijo]()
    
    override init(){
        super.init()
    }
    init (id: String, login: String, passwd: String){
        self.id = id
        self.login = login
        self.passwd = passwd
    }
    
    convenience init(_ dictionary: Dictionary<String, AnyObject>) {
        self.init()
        
        id = dictionary ["id"] as! String
        login = dictionary ["login"] as! String
        nombre = dictionary ["nombre"] as! String
        apellido = dictionary ["apellido"] as! String
        telefono = dictionary ["telefono"] as! String
        passwd = dictionary ["passwd"] as! String
    }
    
    func tieneHijos () -> Bool {
        if hijos.count > 0{
            return true
        }
        return false
    }
    
    

}
