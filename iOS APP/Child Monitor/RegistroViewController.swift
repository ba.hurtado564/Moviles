//
//  RegistroViewController.swift
//  Child Monitor
//
//  Created by Nicolas Ramirez on 6/10/17.
//  Copyright © 2017 Javier Esteban Camargo Urrego. All rights reserved.
//

import UIKit
import Firebase

class RegistroViewController: UIViewController, UINavigationControllerDelegate,UITextFieldDelegate {
    
    var ref: DatabaseReference!

    @IBOutlet weak var textName: UITextField!
    
    @IBOutlet weak var textLastname: UITextField!
    
    @IBOutlet weak var textMail: UITextField!
    
    @IBOutlet weak var textPhone: UITextField!
    
    @IBOutlet weak var textPasswd: UITextField!
    
    @IBOutlet weak var textPasswd2: UITextField!
    
    @IBAction func Registro(_ sender: Any) {
    
        if textName.text == nil || textName.text == ""
        {
            showAlert(title: "Nombre vacío", message: "Ingresar nombre", closeButtonTitle: "cerrar")
            return
        }
        if textLastname.text == nil || textLastname.text == ""
        {
            showAlert(title: "Apellido vacío", message: "Ingresar Apellido", closeButtonTitle: "cerrar")
            return
        }
        if textMail.text == nil || textMail.text == ""
        {
            showAlert(title: "correo vacío", message: "Ingresar correo", closeButtonTitle: "cerrar")
            return
        }else if  !isValidEmail(testStr: textMail.text!){
            return
        }
        
        if textPhone.text == nil || textPhone.text == ""
        {
            showAlert(title: "teléfono vacío", message: "Ingresar teléfono", closeButtonTitle: "cerrar")
            return
        }
        
        if textPasswd.text == nil || textPasswd.text == ""
        {
            showAlert(title: "contraseña vacía", message: "Ingresar contraseña", closeButtonTitle: "cerrar")
            return
        }
        else if textPasswd.text != textPasswd2.text {
            showAlert(title: "contraseñas no coinciden", message: "Repetir contraseña", closeButtonTitle: "cerrar")
            return
        }
        Auth.auth().createUser(withEmail: textMail.text!, password: textPasswd.text!) { (user, error) in
            if let error = error {
                self.showAlert(title: "error", message: error.localizedDescription, closeButtonTitle: "cerrar")
                return
            }
            if let user = user{
                self.ref.child("Usuarios").child(user.uid).setValue(["apellido": self.textLastname.text,"nombre": self.textName.text,"numero": self.textPhone.text])
            }
        }
        self.showAlert(title: "ÉXITO", message: "Usuario fue registrado con éxito", closeButtonTitle: "cerrar")
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func showAlert(title:String,message:String,closeButtonTitle:String)
    {
        let alertController = UIAlertController(title:title,message:message,preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: closeButtonTitle, style: .default)
        {
            (action:UIAlertAction) in
            //ALGO
            
        }
        alertController.addAction(cancelAction)
        self.present(alertController,animated: true)
        {
        }
        
    }
    
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    override func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()

        self.hideKeyboardWhenTappedAround()
        
        self.textName.delegate = self;
        self.textLastname.delegate = self;
        self.textMail.delegate = self;
        self.textPhone.delegate = self;
        self.textPasswd.delegate = self;
        self.textPasswd2.delegate = self;

        // Do any additional setup after loading the view.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
