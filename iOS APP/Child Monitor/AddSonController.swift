//
//  AddSonController.swift
//  Child Monitor
//
//  Created by Javier Camargo Urrego on 9/30/17.
//  Copyright © 2017 Javier Esteban Camargo Urrego. All rights reserved.
//

import UIKit
import MobileCoreServices
import CoreData
import Firebase



class AddSonController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    var usuario: Usuario!
    
    var ref: DatabaseReference!
    
    var son: [NSManagedObject] = []


    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var lblName: UITextField!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var haveSmartphone: UISwitch!
    
    
    var smartphone: Bool!
    var optSelected: String!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }

    @IBAction func stepper(_ sender: UIStepper) {
        lblAge.text = String(Int(sender.value))
      /*  let edad = Int(lblAge.text!)
       if  edad >= 125
        {
            showAlert(title: "Inconsistent Age", message: "You must insert a consistent age", closeButtonTitle: "Closer")
            return
        }
        */
    }
    
    @IBAction func Switch(_ sender: UISwitch)
    {
        if sender.isOn == true
        {
            smartphone = true
        }
        else
        {
            smartphone = false
        }
        print(smartphone)
    }
    
    /*
     if lblAge.text == nil || lblAge.text == ""
     {
     showAlert(title: "Edad Vacia", message: "Debe ingresar la edad de su hijo", closeButtonTitle: "Close")
     return
     }
 */
    var options = ["GPS Monitoring","Beacon Monitoring"]
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return options.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return options[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        optSelected = options [row]
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let mediaType = info [UIImagePickerControllerMediaType] as! NSString
        self.dismiss(animated: true, completion: nil)
        if mediaType.isEqual(to: kUTTypeImage as String){
            let images = info [UIImagePickerControllerOriginalImage] as! UIImage
            image.image = images
        }
    }
    
    
 
    @IBAction func UploadPhoto(_ sender: Any)
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.mediaTypes = [kUTTypeImage as String]
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion:  nil)
        }
    }
    

    @IBAction func TakePhoto(_ sender: Any)
    {        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.camera
        imagePicker.mediaTypes = [kUTTypeImage as String]
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion:  nil)
        }
    }
    

    
    func showAlert(title:String,message:String,closeButtonTitle:String)
    {
        let alertController = UIAlertController(title:title,message:message,preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: closeButtonTitle, style: .default)
        {
            (action:UIAlertAction) in
            //ALGO
            
        }
        alertController.addAction(cancelAction)
        self.present(alertController,animated: true)
        {
        }
        
    }
    

    
    
    @IBAction func AddSon(_ sender: Any) {
        
        if lblAge.text == nil || lblAge.text == ""
        {
            showAlert(title: "Empty Age", message: "You should insert the age of your son", closeButtonTitle: "Close")
            return
        }
        
        if lblName.text == nil || lblName.text == "" 
        {
            showAlert(title: "Empty Name", message: "You should insert the name of your son", closeButtonTitle: "Close")
            return
        }
        // ||  lblName.text.characters.count >60
        
        if optSelected == nil || optSelected == ""
        {
            showAlert(title: "Empty Options", message: "You should select one option for monitoring", closeButtonTitle: "Close")
            return
        }
        nombre = lblName.text!
        
        self.ref.child("Hijos").child(usuario.id).child("hijo\(usuario.hijos.count)").setValue(["nombre": self.lblName.text,"padre": self.usuario.id])
        
        
        // Persistence
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
          return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Son",
                                                in: managedContext)!
        let person = NSManagedObject(entity: entity,
                                     insertInto: managedContext)
        let smartphone = haveSmartphone.isOn
        person.setValue(lblName.text, forKeyPath: "nombre")
            person.setValue(Int(lblAge.text!), forKeyPath: "age")
        person.setValue(smartphone, forKeyPath: "smartphone")
        person.setValue(optSelected, forKeyPath: "typeOfMonitoring")
        
        let hijo = Hijo(nombre: lblName.text!, edad: lblAge.text!, smartphone: smartphone, padre: usuario.id!, TipoMonitoreo: optSelected!)
        usuario.hijos.append(hijo)
        

        do {
          try managedContext.save()
          son.append(person)
        } catch let error as NSError {
          print("Could not save. \(error), \(error.userInfo)")
        }
  

        
        //
        
        let alertController = UIAlertController(title:"The children has been added",message: "Sucess",preferredStyle: .alert)
        let volverAction = UIAlertAction(title: "Volver a Hijos", style:.default)
        {
        (action:UIAlertAction) in
        self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(volverAction)
        self.present(alertController,animated: true){}
        
        
        
    }
    
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    @IBAction func dispose(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        self.hideKeyboardWhenTappedAround()

        self.lblName.delegate = self;

        // Do any additional setup after loading the view.
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoPreviousWithAddedSon"{
            let viewController: MenuControllerFather = segue.destination as! MenuControllerFather
            viewController.usuario = sender as! Usuario
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
