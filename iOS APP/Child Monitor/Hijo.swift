//
//  Hijo.swift
//  Child Monitor
//
//  Created by Nicolas Ramirez on 7/10/17.
//  Copyright © 2017 Javier Esteban Camargo Urrego. All rights reserved.
//

import UIKit

class Hijo: NSObject {

    var nombre: String!
    var edad: String?
    var smartphone: Bool?
    var padre: String!
    var TipoMonitoreo: String!
    
    override init(){
        super.init()
    }
    
    init(nombre: String!, edad: String?, smartphone: Bool?, padre: String!, TipoMonitoreo: String!){
        self.nombre = nombre
        self.edad = edad
        self.smartphone = smartphone
        self.padre = padre
        self.TipoMonitoreo = TipoMonitoreo
    }
    
    convenience init(_ dictionary: Dictionary<String, AnyObject>) {
        self.init()
        
        nombre = dictionary ["nombre"] as! String
        padre = dictionary ["padre"] as! String
    }
    
    

}
