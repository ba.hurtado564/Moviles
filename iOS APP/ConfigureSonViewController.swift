//
//  ConfigureSonViewController.swift
//  Child Monitor
//
//  Created by Nicolas Ramirez on 8/10/17.
//  Copyright © 2017 Javier Esteban Camargo Urrego. All rights reserved.
//

import UIKit

class ConfigureSonViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var usuario: Usuario!
    
    var hijos : [Hijo] = [Hijo] ()

    @IBOutlet weak var listCell: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listCell.delegate = self
        listCell.dataSource = self
        
        cargarHijos()

        // Do any additional setup after loading the view.
    }
    
    func cargarHijos ( ) {
        
        self.hijos = self.usuario.hijos
        self.listCell.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return hijos.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "SonCell", for: indexPath)
        
        let currentHijo: Hijo = hijos[indexPath.row]
        
        print(currentHijo.nombre)
        
        let imageView: UIImageView = cell.viewWithTag(2) as! UIImageView
        //        imageView.setImageWith(URL(string: currentHijo.imagen!)!)
        //
        let labelNombre: UILabel = cell.viewWithTag(1) as! UILabel
        labelNombre.text = currentHijo.nombre!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let son = hijos[indexPath.row]
        defaults.set(son.nombre, forKey: "NombreHijo")
        self.performSegue(withIdentifier: "GoToMainSon", sender: son)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "GoToMainSon"{
            let viewController: MainSonViewController = segue.destination as! MainSonViewController
            viewController.hijo = sender as! Hijo
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
