Proyecto de moviles
# Safe Child

## Overview

Child Monitor is a mobile app that aid the people with childs to have an eye on them. This app let parents to monitor the live movements of their sons and being able to receive notifications about their positions. This app also let the childs to report emergencies if needed.

## Features

Already developed features

| Feature | Description |
| ------------ | -------------
| Register new user	| The parent sign up in the app and is registered in our databases
| Add children	| The parent can add any number of children they want for monitoring and add their pictures
| Set a range to the child	|  The paren can monitor the GPS position of any child and add an safe area in which the child can be
| Beacon monitoring | With the app the parent can monitor the distance at which the child carrying a beacon can be
| Alert function | The child can declare an emergency state in which a notification is send to the parent
| See contacs | The children can look at the contacts in order to make a call in case of emergency

## App Design

### Data
All the data will be persisted in Firebase. The structure used is the next:
- Hijos
  - User ID
        - hijo n
              - edad
              - monitoreo (type of monitoring)
              - nombre
              - padre (parent id)
              - posicion (# of the child)
              - profileImageURL (URL of the image)
              - smartphone (Boolean if child has an smartphone
              - latitud (latitude of the child)
              - longitud (longitude of the child)
              - token (Parent device's token for firebase cloud messaging)
- Usuarios
    - User ID
        - apellido (lastname of parent)
        - id (User's Id)  
        - login (User's mail) 
        - nombre (User's name)
        - numero (User's phone number)
        - passwd (User's password) 
        - token (Device's token)

### Firebase Authentication
Used for the app authentication

### Firebase storage
Space where images are stored

# Milestones for iOS

## Milestone 1 -> Sep 17, 2017–Oct 7
    User's basic logic
|Issue	|Date closed
|--------|---------
|Integration with Firebase Database | Oct 5
| Register User	| Oct 3
| Login	| Oct 3
| Persist data of a child	| Oct 3
| Visualize added children	| Oct 6
| Add Children	| Oct 3
| Visual components of child details	| Oct 7
| Load children	| Oct 6
| Persist sesion	| Oct 7

## Milestone 2 -> Oct 8, 2017–Oct 9, 2017
	Design
|Issue	|Date closed
|--------|---------
| Register finished	| Oct 8
| Error in formularies	| Oct 8
| Add constrains	| Oct 9

## Oct 7
In order to offer the child functionalities the same user login is used.
When the user sign in as child set up the registered child and the app does not let the child to logout

## Milestone 3 -> Oct 8, 2017–Oct 9, 2017
	Advanced functionalities
|Issue	|Date closed
|--------|---------
| Send push notification	| Oct 8
| GPS function	| Oct 8
| Use of firebase tokens	| Oct 8
| Use of Beacons	| Oct 9
| Set safe area	| Oct 9

## Tests -> Oct 9

In an Excel PruebasiOS.xlsx commited to the root the test done are registered


# Members

Javier Esteban Camargo ---> iOS
Juan Pablo Camacho ---> Android
Juan Esteban Molano ---> iOS
Boris Hurtado ---> Android

