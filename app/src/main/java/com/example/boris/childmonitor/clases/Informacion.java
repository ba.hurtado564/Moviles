package com.example.boris.childmonitor.clases;

import android.icu.text.IDNA;

/**
 * Created by Boris Alfredo on 4/10/2017.
 */

public class Informacion {
    public String nombre;
    public String apellido;
    public double numero;
    public Informacion(String nombre,String apellido,double numero){
        this.nombre=nombre;
        this.apellido=apellido;
        this.numero=numero;
    }
}
