package com.example.boris.childmonitor.activities;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.KeyEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.boris.childmonitor.R;
import com.example.boris.childmonitor.clases.InfoHijo;
import com.example.boris.childmonitor.clases.Informacion;
import com.example.boris.childmonitor.clases.ListaHijosDelPadre;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.StrictMath.toIntExact;

public class PerfilFinalActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SensorEventListener{

    private static final float SHAKE_THRESHOLD_GRAVITY = 2.7F;
    private static final int SHAKE_SLOP_TIME_MS = 500;
    private static final int SHAKE_COUNT_RESET_TIME_MS = 3000;
    private long mShakeTimestamp;
    private int mShakeCount;
    private long lastPressedTime;
    private static final int PERIOD = 2000;
    private SensorManager mSensorManager;
    private Sensor mAcc;
    private FirebaseAuth firebaseAuth;
    private FloatingActionButton agregarChino;



    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference mHijoRef = mRootRef.child("Hijos");
    ArrayList listaHijos;
    ListaHijosDelPadre listaHijosDelPadre;
    ArrayList nombres;
    private ListView arregloHijo;
    private TextView emailNav;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_final);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAcc = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        Intent intent = getIntent();
        listaHijos = new ArrayList();
        llenarListaHijos();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        firebaseAuth=FirebaseAuth.getInstance();




        listaHijosDelPadre = new ListaHijosDelPadre();
        agregarChino=(FloatingActionButton)findViewById(R.id.boton_agregar_hijo);
        arregloHijo=(ListView)findViewById(R.id.hijos_array);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
                ;
        if(drawer.isDrawerOpen(GravityCompat.START)){
            emailNav=(TextView)findViewById(R.id.email_drawer);

            emailNav.setText("Holaaaaaaaaaaaaaaaaaaaa");
        }
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View hView =  navigationView.getHeaderView(0);
        emailNav=(TextView)hView.findViewById(R.id.email_drawer);

        emailNav.setText(firebaseAuth.getCurrentUser().getEmail());


        agregarChino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(PerfilFinalActivity.this,AddChildActivity.class );
                startActivity(i);
            }
        });
        arregloHijo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent inte = new Intent(PerfilFinalActivity.this,InfoHijoActivity.class);
                inte.putExtra("name", ((InfoHijo)listaHijos.get(i)).getNombre());
                inte.putExtra("father", firebaseAuth.getCurrentUser().getUid());
                startActivity(inte);
            }
        });


    }
    public void llenarListaHijos(){
        nombres = new ArrayList();
        for(int i=0; i<listaHijos.size(); i++){
            InfoHijo hijoNombre = (InfoHijo) listaHijos.get(i);
            nombres.add(hijoNombre.nombre);
            System.out.println(hijoNombre.nombre + nombres);
        }
        arregloHijo = (ListView)findViewById(R.id.hijos_array);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, nombres );
        arregloHijo.setAdapter(arrayAdapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.perfil_final, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.perfil_log_out) {
            logOut();
            return true;
        }
        if(id==R.id.perfil_complementar_info){
            Intent i = new Intent (this, PerfilActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }
    private void logOut() {
        firebaseAuth.signOut();
        Intent i = new Intent(PerfilFinalActivity.this,Login.class);
        startActivity(i);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
            Intent i = new Intent(PerfilFinalActivity.this,AddChildActivity.class );
            startActivity(i);

        } else if (id == R.id.nav_send) {
            logOut();
            return true;
        }
            else if(id == R.id.tips){
                AlertDialog dialog = createAlertDialog();
                dialog.show();


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    protected void onStart(){
        super.onStart();

        mHijoRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String idPadre = firebaseAuth.getCurrentUser().getUid();
                for (DataSnapshot messageSnapshot: dataSnapshot.getChildren()) {
                    for (DataSnapshot messagePadreSnapshot: messageSnapshot.getChildren()) {
                        if(messageSnapshot.getKey().equals(idPadre)) {

                            String name = (String) messagePadreSnapshot.child("nombre").getValue();
                            String father = (String) messagePadreSnapshot.child("padre").getValue();
                            String monitoreo=(String) messagePadreSnapshot.child("monitoreo").getValue();
                            long edad =(long) messagePadreSnapshot.child("edad").getValue();

                            int edadF= new BigDecimal(edad).intValueExact();
                            boolean smartphone= (boolean) messagePadreSnapshot.child("smartphone").getValue();



                            InfoHijo info = new InfoHijo(father,name,edadF,smartphone,monitoreo, null, null);
                            listaHijos.add(info);

                        }
                    }
                }
                llenarListaHijos();
                listaHijosDelPadre.actualizarHijos(listaHijos);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            switch (event.getAction()) {
                case KeyEvent.ACTION_DOWN:
                    if (event.getDownTime() - lastPressedTime < PERIOD) {
                        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                        homeIntent.addCategory( Intent.CATEGORY_HOME );
                        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(homeIntent);

                    } else {
                        Toast.makeText(getApplicationContext(), "Press again to exit.",
                                Toast.LENGTH_SHORT).show();
                        lastPressedTime = event.getEventTime();
                    }
                    return true;
            }
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAcc, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];
        float gX = x / SensorManager.GRAVITY_EARTH;
        float gY = y / SensorManager.GRAVITY_EARTH;
        float gZ = z / SensorManager.GRAVITY_EARTH;
        // gForce will be close to 1 when there is no movement.
        double gForce = Math.sqrt(gX * gX + gY * gY + gZ * gZ);
        if (gForce > SHAKE_THRESHOLD_GRAVITY) {
            final long now = System.currentTimeMillis();
            // ignore shake events too close to each other (500ms)
            if (mShakeTimestamp + SHAKE_SLOP_TIME_MS > now) {
                return;
            }
            // reset the shake count after 3 seconds of no shakes
            if (mShakeTimestamp + SHAKE_COUNT_RESET_TIME_MS < now) {
                mShakeCount = 0;
            }
            mShakeTimestamp = now;
            mShakeCount++;
            Intent i = new Intent(PerfilFinalActivity.this,AddChildActivity.class );
            startActivity(i);
        }
    }
    public AlertDialog createAlertDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Shake yor device to add a child")
                .setTitle("TIP")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Do something
                    }
                });
        return builder.create();
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}