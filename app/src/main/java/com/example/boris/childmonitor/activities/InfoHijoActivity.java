package com.example.boris.childmonitor.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.boris.childmonitor.R;
import com.example.boris.childmonitor.clases.InfoHijo;
import com.example.boris.childmonitor.clases.ListaHijosDelPadre;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.math.BigDecimal;
import java.util.ArrayList;

public class InfoHijoActivity extends AppCompatActivity {

    private StorageReference mStorageReference;
    ArrayList listaHijos;
    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference mHijoRef = mRootRef.child("Hijos");
    FirebaseAuth firebaseAuth;
    ListaHijosDelPadre listaHijosDelPadre;
    String nombreHijo;
    InfoHijo info;
    TextView nombreH;
    TextView monitoreoH;
    TextView edadH;
    String padre;
    private ImageView foto;
    private Button rotar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_hijo);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        foto=(ImageView)findViewById(R.id.ivFotoPerfil);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();
        firebaseAuth=FirebaseAuth.getInstance();
        listaHijos = new ArrayList();
        nombreHijo = intent.getStringExtra("name");
        padre = intent.getStringExtra("father");
        nombreH = (TextView) findViewById(R.id.tvNombre);
        monitoreoH = (TextView) findViewById(R.id.tvMonitoring);
        edadH = (TextView) findViewById(R.id.tvEdad);
        rotar=(Button)findViewById(R.id.rotar);
        rotar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rotarImagen();
            }
        });
    }

    private void rotarImagen() {
        foto.setRotation(foto.getRotation()+90);
    }


    public void getHijo(){
        nombreH.setText(info.getNombre());
        monitoreoH.setText(info.getMonitoreo());
        edadH.setText(info.getEdad()+"");
    }
    protected void onStart(){
        super.onStart();

        mHijoRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot messageSnapshot: dataSnapshot.getChildren()) {
                    for (DataSnapshot messagePadreSnapshot: messageSnapshot.getChildren()) {
                        if(messageSnapshot.getKey().equals(padre)) {

                            String name = (String) messagePadreSnapshot.child("nombre").getValue();
                            String father = (String) messagePadreSnapshot.child("padre").getValue();
                            String monitoreo=(String) messagePadreSnapshot.child("monitoreo").getValue();
                            long edad =(long) messagePadreSnapshot.child("edad").getValue();
                            int edadF= new BigDecimal(edad).intValueExact();
                            boolean smartphone= (boolean) messagePadreSnapshot.child("smartphone").getValue();
                            if (name.equals(nombreHijo)){
                                info = new InfoHijo(father,name,edadF,smartphone,monitoreo, null, null);
                            }
                        }
                    }
                }
                getHijo();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        FirebaseUser users=firebaseAuth.getCurrentUser();
        mStorageReference= FirebaseStorage.getInstance().getReference().child(new String("Hijos")).child(users.getUid()).child(nombreHijo);
         mStorageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
             @Override
             public void onSuccess(Uri uri) {
                 Picasso.with(InfoHijoActivity.this).load(uri).into(foto);
             }
         });

    }

    @Override
    public void onBackPressed() {

        Intent i = new Intent(InfoHijoActivity.this, PerfilFinalActivity.class);

        startActivity(i);
        finish();
        super.onBackPressed();


    }
}